# Docker-codeaster

![https://img.shields.io/docker/automated/negetem/codeaster.svg](https://img.shields.io/docker/automated/negetem/codeaster.svg)
![https://img.shields.io/docker/stars/negetem/codeaster.svg](https://img.shields.io/docker/stars/negetem/codeaster.svg)
![https://img.shields.io/docker/pulls/negetem/codeaster.svg](https://img.shields.io/docker/pulls/negetem/codeaster.svg)

## Description

Run a specific version of Code_aster in a container

Code_Aster is a free and open source software package for civil and structural engineering, finite element analysis, and numerical simulation in structural mechanics which was originally developed as an in-house application by the French company EDF. It was released under the terms of the GNU General Public License in October 2001.

Code_Aster contains 1,500,000 lines of source code, most of it in Fortran and Python, and is being constantly developed, updated and upgraded with new models. Justifying quality labels required by nuclear industry, most of the fields of the software have been validated by independent comparisons with analytical or experimental results, benchmarks towards other codes. The software is provided with about 2,000 tests: they are devoted to elementary qualification and are useful as examples. The documentation of Code_Aster includes more than 14,000 pages and encompasses user's manuals, theory manuals compiling EDF's know-how in mechanics, example problems, verification manuals. The vast majority of the documentation is in French with computer translations in English also available.

## Development and Testing

### Source Code

The [docker-codeaster source] is hosted on BitBucket.
Clone the project with

```
$ git clone https://negetem@bitbucket.org/negetem/docker-codeaster.git
```

[docker-codeaster source]: https://negetem@bitbucket.org/negetem/docker-codeaster.git

### Running Locally

```
$ docker run -it negetem/codeaster --test forma01a
```

### Testing Locally

Build and run the container with

```
$ docker build -t codeaster:12.6.0-4 --build-arg ASTER_VERSION=12.6.0-4 .
$ docker run -it negetem/codeaster --test forma01a
```

## Contributing

Please submit and comment on bug reports and feature requests.

To submit a patch:

1. Fork it (https://bitbucket.org/negetem/docker-codeaster/fork).
2. Create your feature branch (`git checkout -b my-new-feature`).
3. Make changes.
4. Commit your changes (`git commit -am 'Add some feature'`).
5. Push to the branch (`git push origin my-new-feature`).
6. Create a new Pull Request.

## License

This app is licensed under the MIT license.

## Warranty

This software is provided "as is" and without any express or
implied warranties, including, without limitation, the implied
warranties of merchantibility and fitness for a particular
purpose.